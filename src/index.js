import express from 'express'

const PORT = process.env.PORT || 5000

const app = express()

app.use('/', express.static('client/build'))

app.listen(PORT, () => console.log(`Listening on ${ PORT }`))