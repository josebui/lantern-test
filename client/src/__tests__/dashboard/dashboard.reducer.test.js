import reducer from '../../dashboard/dashboard.reducer.js'
import {
  addWeatherStart,
  addWeatherError,
  addWeatherSuccess,
  removeWeather,
  updateWeatherStart,
  updateWeatherError,
  updateWeatherSuccess
} from '../../dashboard/dashboard.actions.js'

const baseInitialState = {
  weather: {},
  processing: false,
  processingByPlace: {},
  error: null
}

describe('Dashboard Reducer', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(baseInitialState)
  })

  it('Should set processing flag', () => {
    expect(reducer(
      baseInitialState,
      addWeatherStart()
    )).toEqual({
      ...baseInitialState,
      processing: true
    })
  })
  it('Should process add result', () => {
    const initialState = {
      ...baseInitialState,
      processing: true
    }
    expect(reducer(
      initialState,
      addWeatherSuccess({ name: 'test' })
    )).toEqual({
      ...baseInitialState,
      weather: {
        test: {
          name: 'test'
        }
      }
    })
    expect(reducer(
      initialState,
      addWeatherError('Failed to add')
    )).toEqual({
      ...baseInitialState,
      error: 'Failed to add'
    })
  })

  it('Should remove weather', () => {
    const initialState = {
      ...baseInitialState,
      weather: {
        quito: {
          name: 'quito'
        } 
      }
    }
    expect(reducer(
      initialState,
      removeWeather('quito')
    )).toEqual({
      ...initialState,
      weather: {}
    })
    expect(reducer(
      baseInitialState,
      removeWeather('quito')
    )).toEqual(
      baseInitialState
    )
  })

  it('Should process weather update start', () => {
    const initialState = {
      ...baseInitialState,
      weather: {
        quito: {
          name: 'quito'
        } 
      }
    }
    expect(reducer(
      initialState,
      updateWeatherStart('quito')
    )).toEqual({
      ...initialState,
      processingByPlace: {
        quito: true
      }
    })
  })
  it('Should process weather update result', () => {
    const initialState = {
      ...baseInitialState,
      processingByPlace: {
        quito: true
      },
      weather: {
        quito: {
          name: 'quito'
        } 
      }
    }
    expect(reducer(
      initialState,
      updateWeatherError('quito')
    )).toEqual({
      ...initialState,
      processingByPlace: {
        quito: false
      }
    })
    expect(reducer(
      initialState,
      updateWeatherSuccess('quito')
    )).toEqual({
      ...initialState,
      processingByPlace: {
        quito: false
      }
    })
  })
})