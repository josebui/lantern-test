import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import _ from 'lodash'

import dashboard from './dashboard/dashboard.reducer.js'

const rootReducer = combineReducers({ dashboard })


export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState) {
      return JSON.parse(serializedState)
    }
  } catch (error) {
    console.error('Error loading local state', error)
  }
}

export const saveState = (state) => {
  const serializedState = JSON.stringify(state)
  localStorage.setItem('state', serializedState)
}
const persistedState = loadState()

const store = createStore(
  rootReducer,
  persistedState,
  applyMiddleware(thunk)
)

store.subscribe(() => {
  saveState({
    dashboard: _.pick(store.getState().dashboard, ['weather'])
  })
})

export default store