import React, { useRef, useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Container from '@material-ui/core/Container'
import { Typography, Fab, CircularProgress } from '@material-ui/core'

import SearchTwoToneIcon from '@material-ui/icons/SearchTwoTone'

import { getWeather, removeWeather, updateCurrent } from '../dashboard.actions'

import WeatherCard from '../../weather/components/WeatherCard'

const useStyles = makeStyles((theme) => ({
  findFormContainer: {
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },

  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 2),
  },
  cardGrid: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  wrapper: {
    marginLeft: theme.spacing(2),
    margin: theme.spacing(1),
    position: 'relative',
  },
  fabProgress: {
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  }
}))

const FindForm = props => {
  const classes = useStyles()
  const placeInput = useRef(null)

  const onAdd = place => {
    props.getWeather(place)
  }

  useEffect(() => {
    var timerId = setInterval(function() {
      props.updateCurrent()
    }, 1000 * 60)
    return () => {
      clearInterval(timerId)
    }
  })

  return (
    <>
      <div className={classes.findFormContainer}>
        <TextField inputRef={placeInput} label='Location' variant='outlined' />
        <div className={classes.wrapper}>
          <Fab
            className={classes.findFormButton}
            aria-label='Get Weather'
            color='primary'
            disabled={props.processing}
            onClick={() => onAdd(placeInput.current.value)}
          >
              <SearchTwoToneIcon />
          </Fab>
          {props.processing && <CircularProgress color='secondary' size={68} className={classes.fabProgress} />}
        </div>
      </div>
      { !props.error || 
        <Typography variant='caption' display='block' color='error' align='center'>{props.error}</Typography>
      }
    </>
  )
}

const Dashboard = props => {
  const classes = useStyles()

  return (
      <>
        <div className={classes.heroContent}>
          <Container maxWidth='sm'>
            <FindForm {...props} />
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth='md'>
          <Grid container spacing={1}>
            {Object.values(props.weather).map(placeData => (
              <WeatherCard
                key={placeData.name}
                data={placeData}
                processing={props.processingByPlace?.[placeData.name]}
                onDelete={() => props.removeWeather(placeData.name)}
              />
            ))}
          </Grid>
        </Container>
      </>
  )
}

export default connect(
  state => ({
    processing: state.dashboard.processing,
    processingByPlace: state.dashboard.processingByPlace,
    error: state.dashboard.error,
    weather: state.dashboard.weather
  }),
  dispatch => ({
    getWeather: place => { dispatch(getWeather(place)) },
    removeWeather: place => { dispatch(removeWeather(place)) },
    updateCurrent: () => dispatch(updateCurrent())
  })
)(Dashboard)
