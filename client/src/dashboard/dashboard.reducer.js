import _ from 'lodash'

import {
  ADD_WEATHER_START,
  ADD_WEATHER_ERROR,
  ADD_WEATHER_SUCCESS,
  REMOVE_WEATHER,
  UPDATE_WEATHER_ERROR,
  UPDATE_WEATHER_START,
  UPDATE_WEATHER_SUCCESS
} from './dashboard.actions'


const initialState = {
  weather: {},
  processing: false,
  processingByPlace: {},
  error: null
}

const reducer = (state = initialState, action) => {
  const reducers = {
    [ADD_WEATHER_START]: () => ({
      ...state,
      processing: true,
      error: null
    }),
    [ADD_WEATHER_ERROR]: () => ({
      ...state,
      processing: false,
      error: action.payload.error
    }),
    [ADD_WEATHER_SUCCESS]: () => ({
      ...state,
      processing: false,
      error: null,
      weather: {
        ...state.weather,
        [action.payload.data.name]: action.payload.data
      }
    }),
    [REMOVE_WEATHER]: () => ({
      ...state,
      weather: _.chain(state.weather)
        .toPairs()
        .filter(([placeName, data]) => action.payload.placeName !== placeName)
        .fromPairs()
        .value()
    }),
    [UPDATE_WEATHER_START]: () => ({
      ...state,
      processingByPlace: {
        ...state.processingByPlace,
        [action.payload.placeName]: true
      }
    }),
    [UPDATE_WEATHER_ERROR]: () => ({
      ...state,
      processingByPlace: {
        ...state.processingByPlace,
        [action.payload.placeName]: false
      }
    }),
    [UPDATE_WEATHER_SUCCESS]: () => ({
      ...state,
      processingByPlace: {
        ...state.processingByPlace,
        [action.payload.placeName]: false
      }
    })
  }
  return reducers[action.type]?.() || state
}

export default reducer