import * as weatherService from '../weather/weather.service'


export const ADD_WEATHER_START = 'ADD_WEATHER_START'
export const ADD_WEATHER_ERROR = 'ADD_WEATHER_ERROR'
export const ADD_WEATHER_SUCCESS = 'ADD_WEATHER_SUCCESS'
export const REMOVE_WEATHER = 'REMOVE_WEATHER'
export const UPDATE_WEATHER_START = 'UPDATE_WEATHER_START' 
export const UPDATE_WEATHER_ERROR = 'UPDATE_WEATHER_ERROR' 
export const UPDATE_WEATHER_SUCCESS = 'UPDATE_WEATHER_SUCCESS' 

export const addWeatherStart = () => ({
  type: ADD_WEATHER_START
})
export const addWeatherError = error => ({
  type: ADD_WEATHER_ERROR,
  payload: { error }
})
export const addWeatherSuccess = data => ({
  type: ADD_WEATHER_SUCCESS,
  payload: { data }
})

export const removeWeather = placeName => ({
  type: REMOVE_WEATHER,
  payload: { placeName }
})

export const updateWeatherStart = placeName => ({
  type: UPDATE_WEATHER_START,
  payload: { placeName }
})
export const updateWeatherError = placeName => ({
  type: UPDATE_WEATHER_ERROR,
  payload: { placeName }
})
export const updateWeatherSuccess = placeName => ({
  type: UPDATE_WEATHER_SUCCESS,
  payload: { placeName }
})

export const getWeather = place => dispatch => {
  dispatch(addWeatherStart())
  weatherService.getWeather(place)
    .then(response => {
      dispatch(addWeatherSuccess({
        ...response,
        updatedAt: Date.now()
      }))
    })
    .catch(error => {
      dispatch(addWeatherError(error))
    })
}

export const updateCurrent = () => (dispatch, getState) => {
  const state = getState()
  const weather = state.dashboard.weather
  Object.keys(weather).forEach(placeName => {
    dispatch(updateWeatherStart(placeName))
    weatherService.getWeather(placeName)
      .then(response => {
        dispatch(addWeatherSuccess({
          ...response,
          updatedAt: Date.now()
        }))
        dispatch(updateWeatherSuccess(placeName))
      })
      .catch(error => {
        dispatch(updateWeatherError(placeName))
      })
  })
}