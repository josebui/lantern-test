import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'

import IconButton from '@material-ui/core/IconButton'
import HighlightOffTwoToneIcon from '@material-ui/icons/HighlightOffTwoTone'

import windImg from '../icons/wind.svg'
import dropImg from '../icons/drop.svg'

const useStyles = makeStyles((theme) => ({
  tempContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  weatherContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  weatherType: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'start',
    marginRight: theme.spacing(1)
  },
  weatherTypeLabel: {
    marginTop: theme.spacing(1)
  },
  weatherIcon: {
    width: '80px'
  },
  weatherDetailsContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  weatherDetailsItem: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2)
  },
  country: {
    marginLeft: theme.spacing(1)
  },
  cardContent: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'start',
    borderTop: '1px solid #d6d6d6'
  },
  detailsIcon: {
    width: '20px',
    height: '20px',
    marginRight: theme.spacing(1)
  },
  updatedAt: {
    color: '#757575'
  }
}))

const Temperature = props => {
  const classes = useStyles()
  const { data } = props
  const { main: { temp, feels_like: feelsLike } } = data
  return (
    <div className={classes.tempContainer}>
      <Typography variant='caption'>
        feels like <b>{Math.round(feelsLike)}°</b>
      </Typography>
      <Typography variant='h3'>
        {Math.round(temp)}°
      </Typography>

    </div>
  )
}

const WeatherType = props => {
  const classes = useStyles()
  const { data } = props
  const { main: weather, description: weatherDescription } = data.weather[0]
  return (
    <div className={classes.weatherContainer}>
      <div className={classes.weatherType}>
        <Typography variant='caption'>
          {weatherDescription}
        </Typography>
        <Typography className={classes.weatherTypeLabel} variant='h5'>
          {weather}
        </Typography>
      </div>
      
    </div>
  )
}

const WeatherDetails = props => {
  const classes = useStyles()
  const { data } = props
  const { main: { humidity }, wind: { speed: windSpeed } } = data
  return (
    <div className={classes.weatherDetailsContainer}>
      <span className={classes.weatherDetailsItem}>
        <img className={classes.detailsIcon} src={dropImg} alt='logo' />
        <Typography variant='caption'>
          {humidity}%
        </Typography>
      </span>
      <span className={classes.weatherDetailsItem}>
        <img className={classes.detailsIcon} src={windImg} alt='logo' />
        <Typography variant='caption'>
          {Math.round(windSpeed)} km/h
        </Typography>
      </span>
    </div>
  )
}

const UpdateAt = ({ data }) => {
  const classes = useStyles()
  const { updatedAt } = data
  const date = new Date(updatedAt)
  const f = new Intl.DateTimeFormat('en', {
    year: 'numeric',
    month: 'short',
    day: '2-digit',
    hour: 'numeric',
    minute: 'numeric'
  })
  const toDisplay = f.format(date)
  return (
    <Typography variant='caption' className={classes.updatedAt}>Updated at {toDisplay}</Typography>
  )
}

const WeatherCard = props => {
  const classes = useStyles()
  const { data } = props
  const { name: city, sys: { country } } = data

  const loaderVariant = props.processing ? 'indeterminate' : 'determinate'

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.card} variant='outlined'>
        <CardHeader
          action={
            <IconButton aria-label='settings' onClick={props.onDelete}>
              <HighlightOffTwoToneIcon />
            </IconButton>
          }
          title={(
           <>
            <Typography variant='h5' display='inline'>
              {city}
            </Typography>
            <Typography className={classes.country} variant='caption' display='inline'>
              {country}
            </Typography>
           </>
          )}
          subheader={(
            <UpdateAt data={data}/>
          )}
        />
        <LinearProgress variant={loaderVariant} value={0}/>
        <CardContent className={classes.cardContent}>
          <Temperature data={data} />
          <img 
            className={classes.weatherIcon}
            src={`https://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`}
            alt='weather'
          />
          <WeatherType data={data} />
        </CardContent>
        <CardContent className={classes.cardContent}>
            <WeatherDetails data={data} />
        </CardContent>
      </Card>
    </Grid>
  )
}
export default WeatherCard