const SERVICE_URL = 'https://api.openweathermap.org/data/2.5/weather' 
const KEY = '2e2de6d6637513c79ed9a893f44c7038'

export const getWeather = place => fetch(
  `${SERVICE_URL}?q=${place}&appid=${KEY}&units=metric`
)
  .then(res => res.json())
  .then(json => (json.cod !== 200)
    ? Promise.reject(json.message)
    : json
  )